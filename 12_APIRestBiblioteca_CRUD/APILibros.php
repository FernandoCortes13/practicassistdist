<?php

include_once 'libros.php';

class APILibros{

    function obtenerLibros(){
        $libro = new Libros();
        $libros = array();
        $libros["items"] = array();

        $res = $libro->consultaLibros();

        if($res->rowCount()){
            while ($row = $res->fetch(PDO::FETCH_ASSOC)){
    
                $item=array(
                    "id" => $row['id'],
                    "titulo" => $row['titulo'],
                    "autor" => $row['autor'],
                );
                array_push($libros["items"], $item);
            }
        
            echo json_encode($libros);
        }else{
            echo json_encode(array('mensaje' => 'No hay elementos'));
        }
    }

    function introduceLibro($titulo, $autor){
        $libro = new Libros();
        $libro->insertaLibro($titulo,$autor);
    }
}

?>