<?php
    class DB{

        private $servidor;
        private $usuario;
        private $password;
        private $bd;
        private $charset;

        public function __construct(){
            $this->servidor = 'localhost';
            $this->usuario = 'root';
            $this->password = 'contra';
            $this->bd = 'biblioteca';
        }

        function conectar(){

            try{

                $conexion = "mysql:host=".$this->servidor.";dbname=" . $this->bd;

                $opciones = [
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_EMULATE_PREPARES => false,
                ];

                $pdo = new PDO($conexion,$this->usuario, $this->password);
                
                return $pdo;

            }catch(PDOException $e){
                echo 'Error: '. $e->getMessage();
            }
        }








    }


?>