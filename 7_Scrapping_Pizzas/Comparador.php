<?php

    include 'Pizzeria.php';

    $pizzeriaHut;
    $pizzeriaDominos;
    $pizzeriaCaesars;

    if(isset($_POST['btnEnviar'])){
        $tipoPizza = $_POST['pizza'];

        $pizzeriaHut = new Pizzeria('https://fastfoodprecios.mx/pizza-hut-precios/','Pizza Hut');
        $pizzeriaDominos = new Pizzeria('https://fastfoodprecios.mx/dominos-precios/','Domino´s Pizza');
        $pizzeriaCaesars = new Pizzeria('https://fastfoodprecios.mx/little-caesars-precios/','Little Casear´s');

        $pizzeriaHut->obtenerPrecio($tipoPizza);
        $pizzeriaDominos->obtenerPrecio($tipoPizza);
        $pizzeriaCaesars->obtenerPrecio($tipoPizza);
    }

?>