<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Comparador</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>
<body>
    
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark justify-content-center text-white">
        <h1>Comparador de pizzas</h1>
    
    </nav>
    <hr/>
    <div class="card ml-auto mr-auto text-center" style="width: 28rem;">
        <div class="card-body">
            <h5 class="card-title">Datos de comparación</h5>
            <h6 class="card-subtitle mb-2 text-muted">Las pizzerias a considerar son: PizzaHut, Domino´s y LittleCaesar´s</h6>

            <div class="form-group">
                <form method="POST" action="Comparador.php">
                <label for="exampleFormControlSelect1">Elija un tipo de pizza</label>
                <select class="form-control" name="pizza">
                <option value="Hawaian">Hawaiana</option>
                <option value="Pepperoni">Pepperoni</option>
                <option value="Queso">Queso</option>
                <option value="Itali">Italiana</option>
                <option value="Mex">Mexicana</option>
    </select>
                    <hr/>

                    <button class="btn btn-block btn-primary" type="submit" name="btnEnviar">Consultar</button>

                </form>
            </div>    
            
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>
</html>




 