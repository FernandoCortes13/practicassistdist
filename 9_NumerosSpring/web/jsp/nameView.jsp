<%-- 
    Document   : nameView
    Created on : 30/05/2020, 12:03:52 AM
    Author     : ESMERALDA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Escribe el número a traducir</h1>
        <spring:nestedPath path="name">
    <form action="" method="post">
        Número:
        <spring:bind path="value">
            <input type="text" name="${status.expression}" value="${status.value}">
        </spring:bind>
        <input type="submit" value="OK">
    </form>
        </spring:nestedPath>
    </body>
</html>
