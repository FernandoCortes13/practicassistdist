<?php
    class Traductor{

        private $numeros = array(
            1=>'hana',
            2=>'Du',
            3=>'Se',
            4=>'Ne',
            5=>'Daseos',
            6=>'Yug',
            7=>'Ilgob',
            8=>'Yeodeolb',
            9=>'Ahob',
            10=>'Sib'
        );


        function traduce($numero){
            
            return json_encode($this->numeros[$numero]);
        }
    }
?>